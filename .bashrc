# /etc/bashrc - global bashrc for non-interactive shells.


# the scripts that call this script expect a nicely setup environment
# for running scripts, not fancy prompts. So let's be restrictive
# by default:

# this is the minimum and given by init to start scripts (so a good start)
PATH=/usr/local/sbin:/sbin:/bin:/usr/sbin:/usr/bin

# these might not be present, so keep our path clean:
for DIR in /usr/local/bin /usr/X11/bin ; do
    [ -d $DIR ] && PATH="$PATH:$DIR"
done

# bare necessities:
export  PATH LANG=
umask   022

export PATH="$PATH:/usr/games"

alias ls="ls --color=auto"
alias l="ls -lakh"
alias xclip="xclip -selection c"
alias mv="mv -i"
alias nethz="echo $( cat /home/alexander_schoch/Documents/ETHZ/account/nethz ) | xclip"

alias poweroff="sudo poweroff"
alias reboot="sudo reboot"

alias gse="emerge --search"
alias gin="sudo emerge --autounmask-write --ask"
alias grb="sudo emerge -uDU --keep-going --with-bdeps=y @world && sudo emerge --depclean"
alias grm="sudo emerge -cav --ask"
alias gup="sudo emerge --sync && sudo emerge -uDNv world"

alias game='emacs -batch -l dunnet'
alias inet='sudo ip link set wlp59s0 up ; sudo iw dev wlp59s0 connect "Martin Router King" ; sudo dhcpcd wlp59s0'
alias tethering='sudo ip link set enp0s20f0u7 up ; sudo dhcpcd enp0s20f0u7 '
alias eduroam='sudo killall wpa_supplicant && sudo wpa_supplicant -i wlp59s0 -B -c /etc/wpa_supplicant/wpa_supplicant.conf ; sudo dhcpcd wlp59s0'
alias raspi="ssh pi@192.168.1.106"
alias thealt="ssh files@alternative.vsos.ethz.ch"
alias thealt_webserver="ssh project2@thealternative.ch"
alias dmenu='dmenu -i -nb "$( cat ~/.cache/wal/colors | head -n 1 | tail -n 1 )" -nf "$( cat ~/.cache/wal/colors | head -n 8 | tail -n 1 )" -sb "$( cat ~/.cache/wal/colors | head -n 4 | tail -n 1 )"'
alias f='fff'


alias getip='dig @ns1-1.akamaitech.net whoami.akamai.net +short'

export PS1="\[\e[0;31m[\u@\h: \w] \\$>\e[m \]"

export MANPATH="$MANPATH:/usr/local/texlive/2018/texmf-dist/doc/man"
export INFOPATH="$INFOPATH:/usr/local/texlive/2018/texmf-dist/doc/info"
export PATH="$PATH:/usr/local/texlive/2018/bin/x86_64-linux/:/usr/local/texlive/2018/texmf-dist/tex/latex/"



# DEFAULT APPLICATIONS

export TERMINAL=terminator
export EDITOR=/usr/bin/vim
# export TERM=/usr/bin/terminator
export MAIL=/usr/bin/thunderbird
export SHELL=/bin/zsh
#export BROWSER=/usr/bin/qutebrowser
