#!/bin/bash

compton &
xrdb -load ~/.Xdefaults
xset r rate 200 80
xsetroot -cursor_name left_ptr
wal -i $( cat ~/.cache/wal/wal )
