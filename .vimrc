" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2017 Sep 20
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  if has('persistent_undo')
    set undofile	" keep an undo file (undo changes after closing)
  endif
endif

if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  augroup END

else

	" set autoindent		" always set autoindenting on

endif " has("autocmd")

" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval')
  packadd! matchit
endif

""""""""""""""""""""""""""""""""""""
""Own Settings""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""
set tabstop=2
set encoding=utf-8

filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=2
" when indenting with '>', use 4 spaces width
set shiftwidth=2
" On pressing tab, insert 4 spaces
set expandtab

colo zellner
"inoremap {      {}<Left>
"inoremap {<CR>  {<CR>}<Esc>O
"inoremap {{     {
"inoremap {}     {}

"inoremap (			()<Left>
"inoremap (<CR>	(<CR>)<Esc>0
"inoremap ((			(
"inoremap ()			()

"inoremap [      []<Left>
"inoremap [<CR>  [<CR>]<Esc>0
"inoremap [[     [
"inoremap []     []
set nohlsearch
"saving backup files and undo files into ~/.vimtmp
set backupdir=~/.vimtmp,.
set undodir=~/.vimtmp,.

"line numbering and line wrap indentation
  set number
  :highlight LineNr ctermfg=blue
  set breakindent
  let &showbreak='  '
  "set showbreak=------>\  " line up soft-wrap prefix with the line numbers
  "set cpoptions+=n        " start soft-wrap lines (and any prefix) in the line-number area

"""Compile and save the document using Ctrl-Space
  autocmd FileType tex inoremap <C-@> <Esc>:w<Enter>:!lualatex<Space>%<Enter><Enter>
  autocmd FileType tex nnoremap <C-@> :w<Enter>:!lualatex<Space>%<Enter><Enter>

"""Press ; and Space in order to jump to the next placeholder
  inoremap ;<Space> <Esc>/<++><Enter>"_c4l

"""Text formatting: bold, italics and small caps
  autocmd FileType tex inoremap ;b \textbf{}<Space><++><Esc>5hi
  autocmd FileType tex inoremap ;i \textem{}<Space><++><Esc>5hi
  autocmd FileType tex inoremap ;sc \textsc{}<Space><++><Esc>5hi

"""Bracket Completion. Does not really work the way it should.
"inoremap { {}<Space><++><Esc>2hi
  inoremap ( ()<++><Esc>4hi
  inoremap [ []<++><Esc>4hi

"""Random TeX Markos
"tabular
  autocmd FileType tex inoremap ;tab \begin{tabular}{}<Enter><++><Enter>\end{tabular}<Enter><++><Esc>3k$i 
"normal figure with [H] option, centering and installed font
  autocmd FileType tex inoremap ;figure \begin{figure}[H]<Enter>\centering<Enter>\computerFont<Enter><Enter>\end{figure}<Enter><Enter><++><Esc>3ki<Tab><Tab>
"itemize
  autocmd FileType tex inoremap ;itemize \begin{itemize}<Enter>\item<Enter>\end{itemize}<Enter><Enter><++><Esc>3kA<Space>
"aligned equation
  autocmd FileType tex inoremap ;ea \begin{equation}<Enter>\begin{array}{}<Enter><++><Enter>\end{array}<Enter>\end{array}<Enter><Enter><++><Esc>5k$i
"normal equation
  autocmd FileType tex inoremap ;eq \begin{equation}<Enter>\end{equation}<Enter><Enter><++><Esc>3k$o
"figure: image right, caption left
  autocmd FileType tex inoremap ;figr \begin{figure}[H]<Enter>\centering<Enter>\computerFont<Enter>\begin{minipage}{\linewidth}<Enter>\caption{\oldstylenums{<++>}}<Enter>\end{minipage}<Enter>\begin{minipage}{<++>\linewidth}<Enter>\includegraphics[width=\linewidth]{<++>}<Enter>\end{minipage}<Enter>\end{figure}<Enter><Enter><++><Esc>8k$10hi
"figure: image left, caption right
  autocmd FileType tex inoremap ;figl \begin{figure}[H]<Enter>\centering<Enter>\computerFont<Enter>\begin{minipage}{\linewidth}<Enter>\includegraphics[width=\linewidth]{<++>}<Enter>\end{minipage}<Enter>\begin{minipage}{<++>\linewidth}<Enter>\caption{\oldstylenums{<++>}}<Enter>\end{minipage}<Enter>\end{figure}<Enter><Enter><++><Esc>8k$10hi
"section
  autocmd FileType tex inoremap ;sec \section{}<Enter><Enter><++><Esc>2k$i
"subsection
  autocmd FileType tex inoremap ;ssec \subsection{}<Enter><Enter><++><Esc>2k$i
"subsubsection
  autocmd FileType tex inoremap ;sssec \subsubsection{}<Enter><Enter><++><Esc>2k$i
"paragraph
  autocmd FileType tex inoremap ;par \paragraph{}<Space><++><Esc>5hi
"chemical equation as a complete figure 
  autocmd FileType tex inoremap ;figchem \begin{figure}[H]<Enter>\centering<Enter>\computerFont<Enter>\schemestart\footnotesize<Enter><Space><Space>\chemfig{}<Enter><BS><BS>\schemestop<Enter>\caption{\oldstylenums{<++>}}<Enter>\end{figure}<Esc>3k$i
"chemfig
  autocmd FileType tex inoremap ;chem \chemfig{}<Left>
"chemmove: arrows in chemfig figures
  autocmd FileType tex inoremap ;cmove \chemmove{\draw[shorten<Space><=1pt,shorten<Space>>=1pt]<Space>(<++>)..<Space>controls<Space>+(<++>:<++>cm)<Space>and<Space>+(<++>:<++>cm)..<Space>(<++>);}<Esc>0/<Enter>"_c4l

  autocmd FileType * inoremap aa ä
  autocmd FileType * inoremap oo ö
  autocmd FileType * inoremap uu ü


""""""""""""""
"""BibLaTeX"""
""""""""""""""
autocmd FileType bib inoremap ;website @misc{,<Enter>title={{<++>}},<Enter>howpublished={{\url{<++>}}},<Enter>note={{\computerFont Visited on <++>\hoch{th} of <++>, <++>}}<Enter>}<Esc>4k$i

""""""""""
"""html"""
""""""""""
autocmd FileType html inoremap ;html <html><Enter><Enter></html><Esc>ki
autocmd FileType html inoremap ;head <head><Enter><Enter></head><Enter><++><Esc>2ki
autocmd FileType html inoremap ;body <body><Enter><Enter></body><Enter><++><Esc>2ki
autocmd FileType html inoremap ;title <title></title><Enter><++><Esc>k$7hi
autocmd FileType html inoremap ;p <p><Enter><Enter></p><Enter><++><Esc>2ki

""""""""""""""
"""Markdown"""
""""""""""""""

autocmd FileType markdown inoremap <C-@> <Esc>:w<Enter>:!pandoc<Space>%<Space>-o<Space>"$(<Space>echo<Space>%<Space>\|<Space>head<Space>-c<Space>-4<Space>).pdf"<Enter><Enter>
autocmd FileType markdown nnoremap <C-@> :w<Enter>:!pandoc<Space>%<Space>-o<Space>"$(<Space>echo<Space>%<Space>\|<Space>head<Space>-c<Space>-4<Space>).pdf"<Enter><Enter>


"""""""""""""""
"""RMarkdown"""
"""""""""""""""

autocmd FileType rmd inoremap <C-@> <Esc>:w<Enter>:!echo<Space>"require(rmarkdown);<Space>render('%')"<Space>\|<Space>R<Space>--vanilla<Enter>
autocmd FileType rmd nnoremap <C-@> :w<Enter>:!echo<Space>"require(rmarkdown);<Space>render('%')"<Space>\|<Space>R<Space>--vanilla<Enter>


""""""""""""""""
"""processing"""
""""""""""""""""

"autocmd FileType * nnoremap <C-@> :w<Enter>:![[<Space>$(<Space>echo<Space>%<Space>\|<Space>grep<Space>".pde"<Space>)<Space>]]<Space>&&<Space>processing-java<Space>--sketch=$(<Space>pwd<Space>)<Space>--run<Enter>


