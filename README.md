# dotfiles-gentoo

All relevant dotfiles of my Gentoo GNU/Linux System

# Screenshots

![Clean](screenshots/clean.png)
![rofi](screenshots/rofi.png)
![busy](screenshots/busy.png)
![fakebusy](screenshots/fakebusy.png)

